/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _PROJECT_DEFS_H
#define _PROJECT_DEFS_H

#ifdef __SDCC
	#include <STC/8H8KxxU/SKDIP28.h>
#else
	#include <uni-STC/uni-STC.h>
#endif // __SDCC

// player.c needs stopTimer().
#define HAL_TIMER_API_STOP_TIMER
#define HAL_TIMER_API_TIMER_PINS
#define PLAYER_TIMER TIMER2
#define PLAYER_TIMER_PIN_SWITCH 0

// star.c needs pwmEnableChannelOutput() and pwmDisableChannelOutput().
#define HAL_PWM_API_DISABLE
#define HAL_PWM_API_STOP

// buttons.c only needs GPIO_HAS_INT_WK
#ifdef GPIO_HAS_PU_NCS
	#undef GPIO_HAS_PU_NCS
#endif
#ifdef GPIO_HAS_SR_DR_IE
	#undef GPIO_HAS_SR_DR_IE
#endif

#define HAL_UARTS 2
#define SUPPRESS_delay1us_WARNING

#define CONSOLE_SPEED 115200UL
#define CONSOLE_UART UART1
#define CONSOLE_PIN_CONFIG 0

#define APP_TIMER_FREQ_HZ 16ul

#endif // _PROJECT_DEFS_H
