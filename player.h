/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _PLAYER_H
#define _PLAYER_H

/**
 * player.c requires the following definitions in project-defs.h:
 * 
 * #define HAL_TIMER_API_STOP_TIMER
 * #define HAL_TIMER_API_TIMER_PINS
 * #define PLAYER_TIMER              TIMER<n>
 * #define PLAYER_TIMER_PIN_SWITCH   <m>
 * 
 * where:
 * 
 * n = the number of the timer you want to use (0, 1, 2, 3 4, or 11)
 * m = 0 or 1, the pin switch for the T<n>CLKO output. This must be 
 * defined, but is only relevant with T3 or T4 on the STC8H MCU that 
 * have TIMER_HAS_T3_T4_PIN_SWITCH defined (e.g. STC8H3 series).
 */

/**
 * Initialises the player hardware.
 */
void player_initialise();

/**
 * Plays a single note.
 * Frequency is in Hz, and duration in ms.
 */
void playNote(uint16_t frequency, uint16_t duration);

/**
 * Configures the player to play a melody and plays its first note.
 * If autorepeat is true, pause specifies the duration of the silence
 * between 2 successive iterations, in ms.
 */
void playMelody(const uint8_t *melody, bool autoRepeat, uint16_t pause);

/**
 * When playing a melody, plays the next note in the score.
 */
void player_process();

/**
 * @return true if the note or melody is finished playing.
 */
bool playDone();

/**
 * Interrupts the note or melody being played.
 */
void playStop();

#ifndef PLAYER_TIMER
	#error "Please define the PLAYER_TIMER macro in project-defs.h"
	// e.g. #define PLAYER_TIMER TIMER2
#endif

#ifndef PLAYER_TIMER_PIN_SWITCH
	// Pin switching is available only for T3/T4 and only on some STC8H MCU series.
	// Let's select a sensible default in each case.
	#ifdef TIMER_HAS_T3_T4_PIN_SWITCH
		#define PLAYER_TIMER_PIN_SWITCH 1
	#else
		#define PLAYER_TIMER_PIN_SWITCH 0
	#endif
#endif

// Don't forget to include player.h in main.c !
INTERRUPT(player_timer_isr, MAKE_INTERRUPT(PLAYER_TIMER));

#endif // _PLAYER_H
