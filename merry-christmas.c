#include "project-defs.h"
#include "notes.h"

const uint8_t merryChristmas[] = {
	SCORE_BEGIN
	SCORE_BASE_TEMPO(2000)
	SCORE_BASE_OCTAVE(Octave5)
	
	// We wish you a Mer-ry Christ-mas, we wish you a Mer-ry Christ-mas, 
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteGnatural, NoteQuaver)
	SCORE_NOTE(NoteFnatural, NoteQuaver)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteGnatural, NoteQuaver)
	SCORE_NOTE(NoteFnatural, NoteCrotchet)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	
	// we wish you a Mer-ry Christ-mas and a hap-py New Year.
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(+1)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	SCORE_NOTE(NoteFnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteMinim)
	
	// Good ti-dings we bring to you and your kin;
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteFnatural, NoteMinim)
	SCORE_NOTE(NoteFnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteFnatural, NoteCrotchet)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteDnatural, NoteMinim)
	
	// Good ti-dings for Christ-mas and a hap-py New Year.
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(+1)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	SCORE_NOTE(NoteFnatural, NoteCrotchet)
	SCORE_NOTE(NoteGnatural, NoteMinim)
	
	SCORE_END
};
