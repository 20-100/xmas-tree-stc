/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "config.h"
#include <eeprom-hal.h>

#define SETTINGS_START_ADDRESS 0

static bool doLoadConfig = true;
static uint8_t config[SETTING_COUNT];

static void loadConfigIfNeeded() {
	if (doLoadConfig) {
		doLoadConfig = false;
		eepromReadBlock(SETTINGS_START_ADDRESS, config, SETTING_COUNT);
	}
}

uint8_t getConfig(Setting setting, uint8_t defaultValue, uint8_t maxValue) {
	loadConfigIfNeeded();
	uint8_t value = (setting < SETTING_COUNT) ? config[setting] : EEPROM_UNINITIALISED;
	
	// Testing value > maxValue can be useful when the number of choices 
	// has been reduced and the content of the EEPROM becomes out of range.
	return (value == EEPROM_UNINITIALISED || value > maxValue) ? defaultValue : value;
}

void setConfig(Setting setting, uint8_t value) {
	if (setting < SETTING_COUNT) {
		loadConfigIfNeeded();
		config[setting] = value;
		eepromWriteBlock(SETTINGS_START_ADDRESS, config, SETTING_COUNT);
	}
}
