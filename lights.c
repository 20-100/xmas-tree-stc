/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "lights.h"
#include "buttons.h"
#include "config.h"
#include <gpio-hal.h>


#define LIGHTS_COUNT ((uint8_t) (APP_TIMER_FREQ_HZ / 4 /* 1/4 second */))

typedef enum {
	LIGHTS_MODE0,
	LIGHTS_MODE1,
	LIGHTS_MODE2,
	LIGHTS_MODE3,
	LIGHTS_MODE4,
	LIGHTS_MODE5,
	LIGHTS_MODE6,
	
	LIGHTS_FIRST = LIGHTS_MODE0,
	LIGHTS_LAST = LIGHTS_MODE6,
} LightsMode;

static struct {
	uint8_t counter;
	bool doUpdate;
	LightsMode mode;
	const uint8_t *modeTable;
	uint8_t modeSize;
	uint8_t modeIndex;
} lights;

static GpioConfig p1_output = GPIO_PINS_CONFIG(GPIO_PORT1, GPIO_PIN4, 4, GPIO_OPEN_DRAIN_MODE);
static GpioConfig p2_output = GPIO_PORT_CONFIG(GPIO_PORT2, GPIO_OPEN_DRAIN_MODE);
static GpioConfig p3_output = GPIO_PINS_CONFIG(GPIO_PORT3, GPIO_PIN4, 4, GPIO_OPEN_DRAIN_MODE);

/*
 * A mode definition table contains pairs of bytes defining each step of
 * the animation. The first byte defines P17 P16 P15 P14 P37 P36 P35 P34
 * and the second one P27 P26 P25 P24 P23 P22 P21 P20.
 */

// All on
static const uint8_t mode0[] = {
	0x00, 0x00, 
};

// Sweep vertically upwards
static const uint8_t mode1[] = {
	0xef, 0x3e, 0xef, 0x0e, 0xc7, 0x06, 0xc3, 0x02, 0x81, 0x02, 0x81, 0x00, 0x00, 0x00, 
	0x10, 0xc1, 0x10, 0xf1, 0x38, 0xf9, 0x3c, 0xfd, 0x7e, 0xfd, 0x7e, 0xff, 0xff, 0xff, 
};

// Sweep horizontally rightwards
static const uint8_t mode2[] = {
	0xef, 0xf7, 0xab, 0xa7, 0x8a, 0xa4, 0x08, 0x80, 0x00, 0x00, 
	0x10, 0x08, 0x54, 0x58, 0x75, 0x5b, 0xf7, 0x7f, 0xff, 0xff, 
};

// Colour by colour
static const uint8_t mode3[] = {
	0x0f, 0x7f, 0x00, 0x7e, 0x00, 0x00, 
	0xf0, 0x80, 0xff, 0x81, 0xff, 0xff, 
};

// Blink white LED
static const uint8_t mode4[] = {
	0x00, 0x7e, 0x00, 0x3e, 0x00, 0x5e, 0x00, 0x6e, 
	0x00, 0x76, 0x00, 0x7a, 0x00, 0x7c, 
};

// Snake
static const uint8_t mode5[] = {
	0xff, 0xff, 0xef, 0xff, 0xef, 0xbf, 0xef, 0xbe, 0xef, 0x3e, 0xef, 0x1e, 0xef, 0x0e, 0xef, 0x06, 
	0xcf, 0x06, 0xc7, 0x06, 0xc7, 0x02, 0xc3, 0x02, 0xc1, 0x02, 0x81, 0x02, 0x81, 0x00, 0x01, 0x00, 
	0x00, 0x00, 0x10, 0x00, 0x10, 0x40, 0x10, 0x41, 0x10, 0xc1, 0x10, 0xe1, 0x10, 0xf1, 0x10, 0xf9, 
	0x30, 0xf9, 0x38, 0xf9, 0x38, 0xfd, 0x3c, 0xfd, 0x3e, 0xfd, 0x7e, 0xfd, 0x7e, 0xff, 0xfe, 0xff, 
};

// Pacman
static const uint8_t mode6[] = {
	0x00, 0x00, 0x10, 0x00, 0x00, 0x40, 0x00, 0x01, 0x00, 0x80, 0x00, 0x20, 0x00, 0x10, 0x00, 0x08, 
	0x20, 0x00, 0x08, 0x00, 0x00, 0x04, 0x04, 0x00, 0x02, 0x00, 0x40, 0x00, 0x00, 0x02, 0x80, 0x00, 
};

static void initialise() {
	switch (lights.mode) {
	case LIGHTS_MODE0:
		lights.modeTable = mode0;
		lights.modeSize = sizeof(mode0) / sizeof(mode0[0]);
		break;
	
	case LIGHTS_MODE1:
		lights.modeTable = mode1;
		lights.modeSize = sizeof(mode1) / sizeof(mode1[0]);
		break;
	
	case LIGHTS_MODE2:
		lights.modeTable = mode2;
		lights.modeSize = sizeof(mode2) / sizeof(mode2[0]);
		break;
	
	case LIGHTS_MODE3:
		lights.modeTable = mode3;
		lights.modeSize = sizeof(mode3) / sizeof(mode3[0]);
		break;
	
	case LIGHTS_MODE4:
		lights.modeTable = mode4;
		lights.modeSize = sizeof(mode4) / sizeof(mode4[0]);
		break;
	
	case LIGHTS_MODE5:
		lights.modeTable = mode5;
		lights.modeSize = sizeof(mode5) / sizeof(mode5[0]);
		break;
	
	case LIGHTS_MODE6:
		lights.modeTable = mode6;
		lights.modeSize = sizeof(mode6) / sizeof(mode6[0]);
		break;
	}
	
	lights.modeIndex = 0;
}

static void deinitialise() {
	// Turn all LED off
	gpioWrite(&p1_output, 0xff);
	gpioWrite(&p2_output, 0xff);
	gpioWrite(&p3_output, 0xff);
}

// Called from buttons.c
void onLightsModeButton() {
	deinitialise();
	lights.mode = (lights.mode == LIGHTS_LAST) ? LIGHTS_FIRST : (lights.mode + 1);
	setConfig(LIGHTS_MODE, lights.mode);
	initialise();
}

void lights_initialise() {
	gpioConfigure(&p1_output);
	gpioConfigure(&p2_output);
	gpioConfigure(&p3_output);
	lights.doUpdate = false;
	lights.counter = LIGHTS_COUNT;
	lights.mode = getConfig(LIGHTS_MODE, LIGHTS_MODE2, LIGHTS_LAST);
	initialise();
}

void lights_onTimer() {
	if (lights.counter) {
		lights.counter--;
		
		if (lights.counter == 0) {
			lights.doUpdate = true;
		}
	}
}

void lights_process() {
	if (lights.doUpdate) {
		lights.doUpdate = false;
		lights.counter = LIGHTS_COUNT;
		
		gpioWrite(&p1_output, lights.modeTable[lights.modeIndex] >> 4);
		gpioWrite(&p3_output, lights.modeTable[lights.modeIndex]);
		lights.modeIndex++;
		gpioWrite(&p2_output, lights.modeTable[lights.modeIndex]);
		lights.modeIndex++;
		
		if (lights.modeIndex >= lights.modeSize) {
			lights.modeIndex = 0;
		}
	}
}
