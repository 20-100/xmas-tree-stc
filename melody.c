/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "melody.h"
#include "buttons.h"
#include "config.h"
#include "player.h"
#include "jingle-bells.h"
#include "o-tannenbaum.h"
#include "merry-christmas.h"

typedef enum {
	MELODY_TANNENBAUM,
	MELODY_JINGLE_BELLS,
	MELODY_MERRY_XMAS,
	MELODY_SILENCE,
	
	MELODY_FIRST = MELODY_TANNENBAUM,
	MELODY_LAST = MELODY_SILENCE,
} MelodyMode;

static struct {
	MelodyMode mode;
	const uint8_t *score;
} melody;

static void deinitialise() {
	playStop();
}

static void initialise() {
	switch (melody.mode) {
	case MELODY_TANNENBAUM:
		melody.score = oTannenbaum;
		break;
	case MELODY_JINGLE_BELLS:
		melody.score = jingleBells;
		break;
	case MELODY_MERRY_XMAS:
		melody.score = merryChristmas;
		break;
	case MELODY_SILENCE:
		melody.score = NULL;
		break;
	}
	
	if (melody.score != NULL) {
		playMelody(melody.score, true, 1000);
	}
}

void onMelodyModeButton() {
	deinitialise();
	melody.mode = (melody.mode == MELODY_LAST) ? MELODY_FIRST : (melody.mode + 1);
	setConfig(MELODY_MODE, melody.mode);
	initialise();
}

void melody_initialise() {
	player_initialise();
	melody.mode = getConfig(MELODY_MODE, MELODY_TANNENBAUM, MELODY_LAST);
	initialise();
}

void melody_process() {
	player_process();
}
