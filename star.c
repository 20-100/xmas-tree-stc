/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "star.h"
#include "buttons.h"
#include "config.h"
#include <advpwm-hal.h>
#include <gpio-hal.h>


#define GLOW_COUNT ((uint8_t) (APP_TIMER_FREQ_HZ / 8 /* 1/8 second */))
#define BLINK_COUNT ((uint8_t) (APP_TIMER_FREQ_HZ / 2 /* 1/2 second */))

typedef enum {
	STAR_FIXED,
	STAR_GLOWING,
	STAR_BLINKING,
	
	STAR_FIRST = STAR_FIXED,
	STAR_LAST = STAR_BLINKING,
} StarMode;

static struct {
	uint8_t counter;
	bool doUpdate;
	StarMode mode;
	uint8_t blinkCounter;
	bool blinkState;
} star;

static GpioConfig starOutput = GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN3, GPIO_OPEN_DRAIN_MODE);

static const uint16_t GLOW_GRADIENT[] = {
	363, 684, 1159, 1814, 2680, 3785, 5159, 6830, 8827, 11181, 13919, 
	17072, 20668, 24736, 29306, 34407, 40069, 46319, 53187, 60703, 
};

#define GLOW_STEPS (sizeof(GLOW_GRADIENT) / sizeof(GLOW_GRADIENT[0]))
#define GLOW_SIGNAL_FREQ 100ul
#define GLOW_COUNTER PWM_COUNTER_B
#define GLOW_CHANNEL PWM_Channel6
#define GLOW_PIN_CONFIG 1

static int8_t glowStep = 0;
static int8_t glowIncrement = 1;

static void initialise() {
	switch (star.mode) {
	case STAR_GLOWING:
		pwmEnableChannelOutput(GLOW_CHANNEL, PWM_OUTPUT_P_ONLY);
		break;
	
	case STAR_FIXED:
	case STAR_BLINKING:
		gpioWrite(&starOutput, 0);
		break;
	}
}

static void deinitialise() {
	if (star.mode == STAR_GLOWING) {
		pwmDisableChannelOutput(GLOW_CHANNEL, PWM_OUTPUT_P_ONLY);
	}
}

// Called from buttons.c
void onStarModeButton() {
	deinitialise();
	star.mode = (star.mode == STAR_LAST) ? STAR_FIRST : (star.mode + 1);
	setConfig(STAR_MODE, star.mode);
	initialise();
}

void star_initialise() {
	gpioConfigure(&starOutput);
	
	pwmConfigureCounter(
		GLOW_COUNTER, 
		GLOW_SIGNAL_FREQ * 65535UL, 
		GLOW_SIGNAL_FREQ, 
		PWM_FREE_RUNNING, 
		PWM_NO_TRIGGER,
		0, 
		PWM_BUFFERED_UPDATE,
		PWM_CONTINUOUS,
		PWM_EDGE_ALIGNED_UP,
		PWM_DISABLE_ALL_UE,
		DISABLE_INTERRUPT
	);
	
	pwmInitialisePWM(
		GLOW_CHANNEL, 
		OUTPUT_HIGH, 
		DISABLE_INTERRUPT, 
		PWM_IMMEDIATE_UPDATE,
		GLOW_GRADIENT[0]
	);
	
	pwmConfigureOutput(
		GLOW_CHANNEL, 
		GLOW_PIN_CONFIG, 
		GPIO_OPEN_DRAIN_MODE,
		PWM_ACTIVE_LOW, 
		PWM_DISABLE_FAULT_CONTROL, 
		OUTPUT_HIGH,
		PWM_OUTPUT_P_ONLY
	);
	
	// Will be reenabled in initialise() if needed.
	pwmDisableChannelOutput(GLOW_CHANNEL, PWM_OUTPUT_P_ONLY);
	
	pwmEnableMainOutput(GLOW_COUNTER);
	pwmEnableCounter(GLOW_COUNTER);
	
	star.doUpdate = false;
	star.counter = GLOW_COUNT;
	star.mode = getConfig(STAR_MODE, STAR_GLOWING, STAR_LAST);
	star.blinkCounter = BLINK_COUNT;
	star.blinkState = false;
	initialise();
}

void star_onTimer() {
	if (star.counter) {
		star.counter--;
		
		if (star.counter == 0) {
			star.doUpdate = true;
		}
	}
	
	if (star.blinkCounter) {
		star.blinkCounter--;
		
		if (star.blinkCounter == 0) {
			star.blinkState = !star.blinkState;
			star.blinkCounter = BLINK_COUNT;
		}
	}
}

void star_process() {
	if (star.doUpdate) {
		star.doUpdate = false;
		star.counter = GLOW_COUNT;
		
		switch (star.mode) {
		case STAR_GLOWING: {
				pwmSetDutyCycle(GLOW_CHANNEL, GLOW_GRADIENT[glowStep]);
				
				int8_t newStep = glowStep + glowIncrement;
				
				if (newStep < 0 || newStep >= GLOW_STEPS) {
					glowIncrement = -glowIncrement;
				}
				
				glowStep += glowIncrement;
			}
			break;
		
		case STAR_BLINKING:
			gpioWrite(&starOutput, star.blinkState);
			break;
		}
	}
}

#pragma save
// Suppress warning "unreferenced function argument"
#pragma disable_warning 85
void pwmOnCounterInterrupt(PWM_Counter counter, PWM_CounterInterrupt HAL_PWM_SEGMENT event) {
}

void pwmOnChannelInterrupt(PWM_Channel channel, uint16_t HAL_PWM_SEGMENT counterValue) {
}
#pragma restore
