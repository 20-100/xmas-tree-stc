/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "player.h"
#include "notes.h"
#include <timer-hal.h>
#include <gpio-hal.h>

// Default duration of a semibreve in milliseconds
#define SEMIBREVE_MS 400

static struct {
	const uint8_t *score;
	uint16_t scoreIndex;
	uint16_t semibreveDuration;
	NoteOctave baseOctave;
	NoteOctave currentOctave;
	NoteDots noteDots;
	bool hasPlayedNote;
	bool isAutoRepeat;
	bool isRepeating;
	uint16_t pause;
	bool isMelody;
	uint16_t notePeriods;
} player;

static GpioConfig timerOutput = GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN3, GPIO_PUSH_PULL_MODE);

static void noteTerminate() {
	stopTimer(PLAYER_TIMER);
	// Turn buzzer off
	gpioWrite(&timerOutput, 1);
}

INTERRUPT(player_timer_isr, MAKE_INTERRUPT(PLAYER_TIMER)) {
	if (player.notePeriods) {
		player.notePeriods--;
		
		if (!player.notePeriods) {
			// We've reached the note's duration, stop PWM.
			noteTerminate();
		}
	}
}

void player_initialise() {
	player.notePeriods = 0;
	player.isRepeating = false;
	player.isMelody = false;
	
	uint8_t timerPin = getTimerPin(PLAYER_TIMER, TimerOutputPin, PLAYER_TIMER_PIN_SWITCH);
	timerOutput.port = timerPin >> 4;
	timerOutput.pin = timerPin & 0x0f;
	gpioConfigure(&timerOutput);
	// Turn buzzer off
	gpioWrite(&timerOutput, 1);
}

void playNote(uint16_t frequency, uint16_t duration) {
	bool isRest = (frequency == 0);
	
	if (isRest) {
		// frequency == 0 means we're playing a rest.
		// To achieve this, we'll use a frequency of 1kHz,
		// but will keep the PWM output high for the whole
		// duration of the rest.
		frequency = 1000;
	}
	
	uint32_t freq = ((uint32_t) frequency) << 1;
	
	// Determine the number of periods of the output signal
	// corresponding to the specified duration.
	player.notePeriods = (uint16_t) ((((uint32_t) duration) * freq + 500UL) / 1000UL);
	
	startTimer(
		PLAYER_TIMER, 
		frequencyToSysclkDivisor(freq), 
		isRest ? DISABLE_OUTPUT : ENABLE_OUTPUT, 
		ENABLE_INTERRUPT, 
		FREE_RUNNING
	);
}

static void melodyInitialise() {
	player.semibreveDuration = SEMIBREVE_MS;
	player.currentOctave = Octave4;
	player.noteDots = Undotted;
	player.scoreIndex = 0;
	player.hasPlayedNote = false;
}

static void melodyTerminate() {
	player.isMelody = false;
	player.isRepeating = false;
	noteTerminate();
}

void playMelody(const uint8_t *melody, bool autoRepeat, uint16_t pause) {
	player.isMelody = true;
	player.score = melody;
	player.isAutoRepeat = autoRepeat;
	player.pause = pause;
	melodyInitialise();
	player_process();
}

void player_process() {
	if (!player.notePeriods) {
		if (player.isRepeating) {
			player.isRepeating = false;
			melodyInitialise();
			player.isMelody = true;
		}
		
		while (player.isMelody && (player.score[player.scoreIndex] & 0xf0) == 0xf0) {
			switch (player.score[player.scoreIndex]) {
			case 0xff:
				// === 0xff = score start ==================================
				player.scoreIndex++;
				break;
			
			case 0xfe:
				// === 0xfe = score end ====================================
				// Avoid entering an infinite loop if score doesn't contain any playable note.
				if (player.hasPlayedNote && player.isAutoRepeat) {
					player.isRepeating = true;
					
					if (player.pause) {
						playNote(0, player.pause);
					}
					
					// We want to exit the loop but leave the rest of the program state untouched.
					player.isMelody = false;
				} else {
					// will set isMelody to false, causing the loop to end
					melodyTerminate();
				}
				break;
			
			case 0xfd:
				// === 0xfd = set semibreve duration (milliseconds) ========
				player.semibreveDuration = player.score[player.scoreIndex + 1] + (((uint16_t) player.score[player.scoreIndex + 2]) << 8);
				player.scoreIndex += 3;
				break;
			
			case 0xfc:
				// === 0xfc = set dots of the following note ===============
				player.noteDots = (NoteDots) (player.score[player.scoreIndex + 1] & 0x0f);
				player.scoreIndex += 2;
				break;
			
			case 0xfb: {
				// === 0xfb = set current octave relative to base octave ===
					int8_t offset = (int8_t) player.score[player.scoreIndex + 1];
					player.scoreIndex += 2;
					
					// Cap invalid offsets
					if (offset < 0 && ((uint8_t) -offset) > player.baseOctave) {
						offset = -((int8_t) player.baseOctave);
					} else if (offset >= 0 && ((uint8_t) offset) > (Octave9 - player.baseOctave)) {
						offset = Octave9 - player.baseOctave;
					}
					
					// Adjust current octave
					player.currentOctave = player.baseOctave + offset;
				}
				break;
			
			default:
				// === 0xf0..0xfa = set base octave ========================
				player.baseOctave = (NoteOctave) (player.score[player.scoreIndex] & 0x0f);
				player.currentOctave = player.baseOctave;
				player.scoreIndex++;
				break;
			}
		}
		
		if (player.isMelody && (player.score[player.scoreIndex] & 0xf0) != 0xf0) {
			NoteName noteName = (NoteName) (player.score[player.scoreIndex] >> 4);
			NoteValue noteValue = (NoteValue) (player.score[player.scoreIndex] & 0x0f);
			playNote(
				noteFrequency(noteName, player.currentOctave),
				noteDuration(noteValue, player.noteDots, player.semibreveDuration)
			);
			player.hasPlayedNote = true;
			player.noteDots = Undotted;
			player.scoreIndex++;
		}
	}
}

bool playDone() {
	return !player.isMelody && !player.notePeriods;
}

void playStop() {
	melodyTerminate();
	player.notePeriods = 0;
}
