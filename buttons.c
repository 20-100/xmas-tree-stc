/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "buttons.h"
#include <gpio-hal.h>

static GpioConfig starModeButton = {
	.port = GPIO_PORT1, .pin = GPIO_PIN0, .count = 1, 
	.pinMode = GPIO_HIGH_IMPEDANCE_MODE, 
	.pinInterrupt = ENABLE_GPIO_PIN_INTERRUPT, 
	.interruptTrigger = GPIO_FALLING_EDGE, 
	.wakeUpInterrupt = DISABLE_GPIO_PIN_WAKE_UP,
};

static GpioConfig lightsModeButton = {
	.port = GPIO_PORT1, .pin = GPIO_PIN1, .count = 1, 
	.pinMode = GPIO_HIGH_IMPEDANCE_MODE, 
	.pinInterrupt = ENABLE_GPIO_PIN_INTERRUPT, 
	.interruptTrigger = GPIO_FALLING_EDGE, 
	.wakeUpInterrupt = DISABLE_GPIO_PIN_WAKE_UP,
};

static GpioConfig melodyModeButton = GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN2, GPIO_HIGH_IMPEDANCE_MODE);

void buttons_initialise() {
	gpioConfigure(&starModeButton);
	gpioConfigure(&lightsModeButton);
	gpioConfigure(&melodyModeButton);
	
	// INT0 triggers on falling edge
	INT0TR = INTxTR_FALLING_EDGE;
	// Enable INT0
	INT0IE = 1;
}

INTERRUPT(port1_isr, P1_INTERRUPT) {
	uint8_t pintf = P1INTF;
	
	if (pintf & starModeButton.__setMask) {
		pintf &= starModeButton.__clearMask;
		onStarModeButton();
	}
	
	if (pintf & lightsModeButton.__setMask) {
		pintf &= lightsModeButton.__clearMask;
		onLightsModeButton();
	}
	
	P1INTF = pintf;
}

INTERRUPT(extint0_isr, EXTINT0_INTERRUPT) {
	onMelodyModeButton();
	// INT0 interrupt flag is automatically cleared on RETI.
}
