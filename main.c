/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#define _MAIN_C
#include "project-defs.h"
#include "buttons.h"
#include "player.h"
#include "melody.h"
#include "star.h"
#include "lights.h"
#include <gpio-hal.h>
#include <timer-hal.h>

/*
#include <uart-hal.h>
#include <serial-console.h>
#include <stdio.h>
*/
INTERRUPT(timer3_isr, TIMER3_INTERRUPT) {
	lights_onTimer();
	star_onTimer();
}

void main() {
	INIT_EXTENDED_SFR()
	
/*
	serialConsoleInitialise(
		CONSOLE_UART, 
		CONSOLE_SPEED, 
		CONSOLE_PIN_CONFIG
	);
*/

	// Enable interrupts -----------------------------------------------
	EA = 1;
	
	// Initialise modules ----------------------------------------------
	buttons_initialise();
	lights_initialise();
	melody_initialise();
	star_initialise();
	
	// Start application timer -----------------------------------------
	startTimer(
		TIMER3, 
		frequencyToSysclkDivisor(APP_TIMER_FREQ_HZ), 
		DISABLE_OUTPUT, 
		ENABLE_INTERRUPT, 
		FREE_RUNNING
	);
	
	// Main loop -------------------------------------------------------
	while (1) {
		lights_process();
		melody_process();
		star_process();
	}
}
