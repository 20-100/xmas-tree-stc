#include "project-defs.h"
#include "notes.h"

const uint8_t oTannenbaum[] = {
	SCORE_BEGIN
	SCORE_BASE_TEMPO(2250)
	SCORE_BASE_OCTAVE(Octave5)
	
	// Mon beau sapin, 
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	// roi des forêts, 
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteCsharp, NoteSemiQuaver)
	SCORE_NOTE(NoteCsharp, NoteCrotchet)
	// que j'aime ta verdure !
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	// Quand par l'hiver
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteEnatural, NoteQuaver)
	SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteFsharp, NoteCrotchet)
	// bois et guérets
	SCORE_NOTE(NoteEnatural, NoteQuaver)
	SCORE_NOTE(NoteEnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteDnatural, NoteCrotchet)
	// sont dépouillés
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteEnatural, NoteCrotchet)
	// de leurs attraits,
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteCsharp, NoteCrotchet)
	// mon beau sapin, 
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteEnatural, NoteCrotchet)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	// roi des forêts, 
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_DOTS(SingleDotted) SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteCsharp, NoteSemiQuaver)
	SCORE_NOTE(NoteCsharp, NoteCrotchet)
	// tu gardes ta parure.
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteCsharp, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	
	SCORE_END
};
