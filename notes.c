/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include "notes.h"

static const uint32_t octave9FreqX100[] = {
	 837202ul, //  0: C
	 886984ul, //  1: C#
	 939728ul, //  2: D
	 995606ul, //  3: D#
	1054808ul, //  4: E
	1117530ul, //  5: F
	1183982ul, //  6: F#
	1254386ul, //  7: G
	1328976ul, //  8: G#
	1408000ul, //  9: A
	1491724ul, // 10: A#
	1580426ul, // 11: B
};

/**
 * The function's return value is expressed in Hz.
 */
uint16_t noteFrequency(NoteName noteName, NoteOctave octave) {
	switch (noteName) {
	case NoteRest:
		return 0;
	
	case NoteCflat:
		if (octave != Octave_1) {
			noteName = NoteBnatural;
			octave--;
		} else {
			noteName = NoteCnatural;
		}
		break;
	
	case NoteBsharp:
		if (octave != Octave9) {
			noteName = NoteCnatural;
			octave++;
		} else {
			noteName = NoteBnatural;
		}
		break;
	}
	
	uint32_t freqX100 = octave9FreqX100[noteName];
	
	while (octave < Octave9) {
		freqX100 >>= 1;
		octave++;
	}
	
	return (uint16_t) ((freqX100 + 50) / 100L);
}

/**
 * semibreveDuration and the function's return value are expressed in ms.
 */
uint16_t noteDuration(NoteValue noteValue, NoteDots noteDots, uint16_t semibreveDuration) {
	uint16_t result = semibreveDuration;
	
	switch (noteValue) {
	case NoteLonga:
		result <<= 2;
		break;
	case NoteBreve:
		result <<= 1;
		break;
	case NoteSemibreve:
		break;
	case NoteMinim:
		result >>= 1;
		break;
	case NoteCrotchet:
		result >>= 2;
		break;
	case NoteQuaver:
		result >>= 3;
		break;
	case NoteSemiQuaver:
		result >>= 4;
		break;
	case NoteDemiSemiQuaver:
		result >>= 5;
		break;
	case NoteHemiDemiSemiQuaver:
		result >>= 6;
		break;
	}
	
	for (uint16_t extension = result; noteDots > Undotted; noteDots--) {
		extension >>= 1;
		result += extension;
	}
	
	return result;
}
