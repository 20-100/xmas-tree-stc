#include "project-defs.h"
#include "notes.h"

const uint8_t jingleBells[] = {
	SCORE_BEGIN
	SCORE_BASE_TEMPO(2250)
	SCORE_BASE_OCTAVE(Octave5)
	
	// Jingle bells,
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	// jingle bells,
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	// jingle all the way.
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_DOTS(SingleDotted)
	SCORE_NOTE(NoteGnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(1)
	// Oh what fun it is to ride in a
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_DOTS(SingleDotted)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_NOTE(NoteCnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteBnatural, NoteSemiQuaver)
	// one horse open sleigh.
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteCrotchet)
	// O!
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteDnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(0)
	// Jingle bells,
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	// jingle bells,
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	// jingle all the way.
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(1)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_DOTS(SingleDotted)
	SCORE_NOTE(NoteGnatural, NoteQuaver)
	SCORE_NOTE(NoteAnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_NOTE(NoteBnatural, NoteCrotchet)
	SCORE_SHIFT_OCTAVE(1)
	// Oh what fun it is to ride in a
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_DOTS(SingleDotted)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_NOTE(NoteCnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteQuaver)
	SCORE_NOTE(NoteBnatural, NoteSemiQuaver)
	SCORE_NOTE(NoteBnatural, NoteSemiQuaver)
	SCORE_SHIFT_OCTAVE(1)
	// one horse open sleigh.
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteDnatural, NoteQuaver)
	SCORE_NOTE(NoteCnatural, NoteQuaver)
	SCORE_SHIFT_OCTAVE(0)
	SCORE_NOTE(NoteAnatural, NoteQuaver)
	SCORE_NOTE(NoteGnatural, NoteCrotchet)
	
	SCORE_END
};
