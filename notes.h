/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _NOTES_H
#define _NOTES_H

typedef enum {
	NoteCnatural,	// Do
	NoteCsharp,		// Do#
	NoteDnatural,	// Ré
	NoteDsharp,		// Ré#
	NoteEnatural,	// Mi
	NoteFnatural,	// Fa
	NoteFsharp,		// Fa#
	NoteGnatural,	// Sol
	NoteGsharp,		// Sol#
	NoteAnatural,	// La
	NoteAsharp,		// La#
	NoteBnatural,	// Si
	// Special cases
	NoteRest,	// Pauses (silence, soupir, etc.)
	NoteCflat,  // = NoteBnatural of previous octave
	NoteBsharp, // = NoteCnatural of next octave
	// Aliases
	NoteDflat = NoteCsharp,
	NoteEflat = NoteDsharp,
	NoteFflat = NoteEnatural,
	NoteEsharp = NoteFnatural,
	NoteGflat = NoteFsharp,
	NoteAflat = NoteGsharp,
	NoteBflat = NoteAsharp,
} NoteName;

// When using a piezoelectric buzzer, octave should be
// comprised between 1 an 7 inclusive.
typedef enum {
	Octave_1,
	Octave0,
	Octave1,
	Octave2,
	Octave3,
	Octave4,
	Octave5,
	Octave6,
	Octave7,
	Octave8,
	Octave9,
} NoteOctave;

typedef enum {
	NoteLonga, // For rests only
	NoteBreve,		// Carrée  | Bâton de pause
	NoteSemibreve,	// Ronde   | Pause
	NoteMinim,		// Blanche | Demi-pause
	NoteCrotchet,	// Noire   | Soupir
	NoteQuaver,		// Croche  | Demi-soupir
	NoteSemiQuaver,
	NoteDemiSemiQuaver,
	NoteHemiDemiSemiQuaver,
} NoteValue;

typedef enum {
	Undotted,
	SingleDotted,
	DoubleDotted,
	TripleDotted,
	QuadrupleDotted,
} NoteDots;

uint16_t noteFrequency(NoteName noteName, NoteOctave octave);

// semibreveDuration is the duration of a semibreve in milliseconds.
uint16_t noteDuration(NoteValue noteValue, NoteDots noteDots, uint16_t semibreveDuration);

// === Helper macros for score definition ==============================
#define SCORE_BEGIN 0xff,
// duration is in milliseconds
#define SCORE_BASE_TEMPO(duration) 0xfd, ((uint8_t) duration), (duration >> 8),
#define SCORE_BASE_OCTAVE(octave) (0xf0 | octave),
// offset is relative to BASE_OCTAVE
#define SCORE_SHIFT_OCTAVE(offset) 0xfb, ((uint8_t) offset),
// SCORE_DOTS() must *precede* the dotted note.
#define SCORE_DOTS(dots) 0xfc, dots,
#define SCORE_NOTE(name, value) ((name << 4) | value),
#define SCORE_REST(value) ((NoteRest << 4) | value),
#define SCORE_END 0xfe,

#endif // _NOTES_H
